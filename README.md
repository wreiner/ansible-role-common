# Common
This is the common ansible role of wreiner.at infrastracture.

## What tasks are performed by this role?
### Distribution Family specific tasks
If common tasks for a specific distribution family need to be run, the
file _distribution-family-common.yml_ will be included and run.

At the moment there is only a specific file for RedHat distributions,
which installs epel release, disables selinux and reboots the system.
After the reboot _firewalld_ will be installed.

### Install basic packages
The following packages will be installed:
- vim
- nmap
- tmux
- sudo

### Installation of additional packages

Sometimes additional packages should be installed for a system but those
packages do not need any further configuration. Therefor defining a role for
every such package is not practicable.

Those packages can be defined through the variable _common__host_packages_.

```
common__host_packages:
  - screen
```

### sudo
The task _Let wheel group use sudo for all commands_ will in default
config create a sudoers.d file which lets users of the group _wheel_
run every command with sudo but only with password. To change this
behaviour so that users of the wheel group can run commands without
a password set the variable _common__sshd_wheel_password_ to false.

```
# cat host_vars/hostname.yml
common__sudo_wheel_password: false
```

### Create ansible system user
The user _ansbile_ will be created, or the password will be locked
if the user already exists on the system. A public key for this user
will be transferred. To specify the key use the variable
_common__ansible_public_key_.

```
# cat host_vars/hostname.yml
common__ansible_public_key: |
  ssh-rsa AAAAB3NzaC1yc2EA[..]
+UKFjQ7f4NvWVl0HZw/6m7h6Bc[..]
9uYolL9bsCraGyJEJwT8/ww522[..] wreiner@wreinerws
```
